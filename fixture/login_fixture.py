import pytest


@pytest.fixture()
def driver():
    from selenium import webdriver
    from selenium.webdriver.chrome.service import Service
    from webdriver_manager.chrome import ChromeDriverManager
    # a code you write here will run be fore any test case start runing
    my_driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    yield my_driver # your any test will be run right here
    # any code you write after yield will run after your test complete
    my_driver.close()