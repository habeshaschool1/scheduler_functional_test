from time import sleep

from selenium.webdriver.common.by import By

import pytest
from selenium.webdriver.support.select import Select

from fixture.login_fixture import driver
from selector.css_book_appointment import *

def test_search_organization_by_city(driver):
    """ Search organization by city should  organizations they are under that city
    Steps:
        Open book appointemnt page http://3.142.140.217/
        Select City radio box ==> you should get all the cities in city list box
        Select "Sliver Sprint" from the list"
        Then you should get "xyz" org from the organization list box
        """
    driver.get("http://3.142.140.217/")
    element_city = driver.find_element(By.CSS_SELECTOR, css_city_radio_box)

    # check that the city radio box has been selected by default
    selected = element_city.is_selected()
    displayed = element_city.is_displayed()
    assert selected is True
    assert displayed is True

    # check the lest box contains all cities, not organizations type
    element_list = driver.find_element(By.CSS_SELECTOR, css_search_orgs)
    select = Select(element_list)
    my_city = 'Berkeley'
    select.select_by_visible_text(my_city)
    selected_text = select.first_selected_option.text
    assert my_city in selected_text

