from time import sleep

from selenium.webdriver.common.by import By

import pytest

from fixture.login_fixture import driver
from selector.css_login import *


def test_login_success(driver):

    # login success test
    # open the scheduler login page
    driver.get("http://3.142.140.217/login")
    # type correct email and password to the email and password filed
    driver.find_element(By.CSS_SELECTOR, css_email).send_keys("scheduler200@gmail.com")
    driver.find_element(By.CSS_SELECTOR, css_password).send_keys("a")
    # click loing button
    driver.find_element(By.CSS_SELECTOR, css_submit).click()
    # url should contain ==> organizations_appointment_page
    assert "organizations_appointment_page" in driver.current_url

    # then login should be successful and I should see "success: welcome"


def test_login_fail(driver):

    # login fail test
    # open the scheduler login page
    driver.get("http://3.142.140.217/login")
    # type correct email and password to the email and password filed
    driver.find_element(By.CSS_SELECTOR, css_email).send_keys("scheduler200@gmail.com")
    driver.find_element(By.CSS_SELECTOR, css_password).send_keys("aasdsdfasdfasdfasdf")
    # click loing button
    driver.find_element(By.CSS_SELECTOR, css_submit).click()
    # url should contain ==> organizations_appointment_page
    assert "organizations_appointment_page" not in driver.current_url

